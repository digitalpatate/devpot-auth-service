const swaggerUi = require('swagger-ui-express');
const swaggerJsdoc = require('swagger-jsdoc');

const options = {
  swaggerDefinition: {
    info: {
      title: 'Devpot auth-service API',
      version: '1.0.0',
      description: 'Documentation for the auth-service service',
    },
    basePath: process.env.SWAGGER_BASEPATH,
    securityDefinitions: {
      JWT: {
        type: 'apiKey',
        in: 'header',
        name: 'Authorization',
      },
    },
  },
  apis: ['controllers/authController.js', 'controllers/userController.js'],
};

const specs = swaggerJsdoc(options);

module.exports = (app) => {
  app.use(`${process.env.NODE_BASEPATH}/api-docs`, swaggerUi.serve, swaggerUi.setup(specs));
};
