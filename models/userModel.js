const mongoose = require('mongoose');
require('mongoose-type-email');

const { Schema } = mongoose;
const UserSchema = new Schema({
  username: {
    type: String,
    required: 'Username is required',
    unique: true,
  },
  email: {
    type: mongoose.SchemaTypes.Email,
    required: 'A valid email is required',
    unique: true,
  },
  password: {
    type: String,
    required: 'You should provide a password',
  },
  status: {
    type: String,
    enum: ['pending-validate', 'validated', 'pending-reset', 'pending-removed', 'removed'],
    default: 'pending-validate',
  },
});


module.exports = mongoose.model('Users', UserSchema);
