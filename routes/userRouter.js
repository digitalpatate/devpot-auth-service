const router = require('express').Router();
const controller = require('../controllers/userController.js');

const { checkToken } = require('../middleware/checkToken');

router.get('/:id', checkToken, controller.userGet);
router.put('/:id', checkToken, controller.userUpdate);
router.delete('/:id', checkToken, controller.userDelete);

router.post('/:id/reset', controller.resetPassword);
router.post('/:id/tokens/validate', controller.validate);

router.post('/:id/tokens/renew', controller.renewToken);

router.post('/reset', controller.resetPasswordChangeStatus);

module.exports = router;
