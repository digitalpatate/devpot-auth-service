# Devpot - auth-Service

## Description

A NodeJS RESTful API with mongoDB for the devpot project.
It handle the authentification process

## Requirements

- [MongoDB](https://docs.mongodb.com/manual/installation/) >= v3.6.3

## Installation

1. Clone the repo `git clone https://gitlab.com/digitalpatate/devpot-auth-service`
2. Go int the folder `cd devpot-auth-service` run an `npm install`
3. create a `.env` file with the environment variables see the section below
4. Run it with `npm start`

### Or with docker

1. Update if needed the environment variables set in the `Dockerfile`
2. `docker build -t <tag> .`
3. `docker run -d <tag> `

You'll need to bind your local port to the port specified in the `Dockerfile` file 

## Environment variables

- DB_URL: The url of the mongoDB instance

- DB_PORT: The port of the mongoDB instance

- DB_NAME: The database name

- PORT: The port where you will access the service, Where the node server is listening

- NODE_ENV: The mode of the application if not specified it run with the `dev` default value. The possible value can be `dev`, `test`, or whatever for production mode.

- NODE_BASEPATH: The root path of the api

- JWT_SECRET: The secret jwt key to verify signature, should be the same wich you create your jwt 

- JWR_VALIDITY : The validity time for the token.

  ```
  Eg: 1000, "2 days", "10h", "7d". A numeric value is interpreted as a seconds count. If you use a string be sure you provide the time units (days, hours, etc), otherwise milliseconds unit is used by default ("120" is equal to "120ms").
  ```

- MAIL_ADDR: The email address of your mail provider used to send validation link

- MAIL_HOST: Email provider's host

- MAIL_PORT: Email provider's port

- MAIL_USER and MAIL_PWSD: The credentials

- SWAGGER_BASEPATH: The api base path configuration for Swagger, It's used when the base path is different then the actual url. In our case when we run it with traefik. With the url rewriting the base path is different.

## Documentation

Once running, you can access to detailed documentation created with Swagger and openApi, on base path `/api-docs`

```
/!\ Warning /!\ With the deadline approching the Swagger configuration has not been updated with the latest features and bug fixes, some routes has changed.
```

## Database schema

### User

| Name               | Type     | Unique | Required |
| ------------------ | -------- | ------ | -------- |
| identifier         | ObjectId | X      | X        |
| username           | String   | X      | X        |
| email              | String   | X      | X        |
| password           | String   |        | X        |





