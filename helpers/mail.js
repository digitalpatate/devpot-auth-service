const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PWSD,
  },
});

const sendEmail = (to, subject, htmlContent) => {
  if (process.env.NODE_ENV === 'test') return 'ok';
  const mailOptions = {
    from: process.env.MAIL_ADDR,
    to,
    subject,
    html: htmlContent,
  };
  transporter.sendMail(mailOptions, (error) => {
    if (error) {
      return console.log(error);
    }
    return 'ok';
  });
  return null;
};

const sendValidationToken = (email, subject, frontUrl, userId, validationToken) => {
  const validationLink = `${frontUrl}?id=${userId}&token=${validationToken}`;
  sendEmail(email, subject, `<a href="${validationLink}">Valider mon email</a>`);
};
module.exports = {
  sendValidationToken,
};
