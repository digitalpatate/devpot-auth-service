process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const memoryServer = require('mongodb-memory-server');

const User = require('../models/userModel');

const app = require('../app');

chai.use(chaiHttp);

const MongoMemoryServer = new memoryServer.MongoMemoryServer();

const { expect } = chai;
const { should } = chai;

before(async () => {
  const memoryServerInfo = await MongoMemoryServer.getConnectionString();
  await mongoose.connect(memoryServerInfo, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  }, (err) => {
    if (err) console.error(err);
  });
});

after(async () => {
  await mongoose.disconnect();
  await MongoMemoryServer.stop();
});

const userData = {
  username: 'testUserLogin',
  password: 'password',
  email: 'test@test.com',
  hash: '',
};


describe('Tests for /login route', () => {
  describe('User: "pending-validate"', () => {
    beforeEach((done) => {
      userData.hash = bcrypt.hashSync(userData.password, 8);
      newUser = new User({
        username: userData.username,
        email: userData.email,
        password: userData.hash,
        status: 'pending-validate',
      });
      newUser.save((err) => {
        if (err) console.log(err);
      });
      done();
    });

    it('Should not login a user and return a error message', (done) => {
      chai.request(app)
        .post('/login')
        .send({ username: userData.username, password: userData.password })
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(400);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('message');
          expect(res.body)
            .have
            .property('error')
            .eq(true);
          done();
        });
    });
  });
  describe('User: "validated"', () => {
    let newUser;
    before((done) => {
      userData.hash = bcrypt.hashSync(userData.password, 8);
      newUser = new User({
        username: userData.username,
        email: userData.email,
        password: userData.hash,
        status: 'validated',
      });
      newUser.save((err) => {
        if (err) console.log(err);
        done();
      });
    });

    it('Should login a user and return a token', (done) => {
      chai.request(app)
        .post('/login')
        .send({ username: userData.username, password: userData.password, email: userData.email })
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(200);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('token');
          expect(res.body)
            .have
            .property('error')
            .eq(false);
          done();
        });
    });
  });
  describe('User: "pending-reset"', () => {
    beforeEach((done) => {
      userData.hash = bcrypt.hashSync(userData.password, 8);
      newUser = new User({
        username: userData.username,
        email: userData.email,
        password: userData.hash,
        status: 'pending-reset',
      });
      newUser.save((err) => {
        if (err) console.log(err);
      });
      done();
    });

    it('Should not login a user and return a error message', (done) => {
      chai.request(app)
        .post('/login')
        .send({ username: userData.username, password: userData.password })
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(400);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('message');
          expect(res.body)
            .have
            .property('error')
            .eq(true);
          done();
        });
    });
  });
  describe('User: "pending-removed"', () => {
    beforeEach((done) => {
      userData.hash = bcrypt.hashSync(userData.password, 8);
      newUser = new User({
        username: userData.username,
        email: userData.email,
        password: userData.hash,
        status: 'pending-removed',
      });
      newUser.save((err) => {
        if (err) console.log(err);
      });
      done();
    });

    it('Should not login a user and return a error message', (done) => {
      chai.request(app)
        .post('/login')
        .send({ username: userData.username, password: userData.password })
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(400);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('message');
          expect(res.body)
            .have
            .property('error')
            .eq(true);
          done();
        });
    });
  });
  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
