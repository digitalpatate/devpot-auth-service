process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const User = require('../models/userModel');

const app = require('../app');

chai.use(chaiHttp);

const { expect } = chai;
const { should } = chai;
const userData = {
  username: 'testUserLogin',
  password: 'password',
  email: 'test@test.com',
  hash: '',
};


describe('Tests for /register route', () => {
  it('it try to register a user without password', (done) => {
    chai.request(app)
      .post('/register')
      .send({ username: userData.username, email: userData.email })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('it try to register a user without username', (done) => {
    chai.request(app)
      .post('/register')
      .send({ password: userData.password, email: userData.email })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('it try to register a user without email', (done) => {
    chai.request(app)
      .post('/register')
      .send({ username: userData.username, password: userData.password })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  describe('Try register', () => {
    before((done) => {
      userData.hash = bcrypt.hashSync(userData.password, 8);
      const newUser = new User({
        username: userData.username,
        email: userData.email,
        password: userData.hash,
        status: 'pending-reset',
      });
      newUser.save((err) => {
        if (err) console.log(err);
      });
      done();
    });

    it('it should create a new user without error', (done) => {
      chai.request(app)
        .post('/register')
        .send({
          username: userData.username,
          email: userData.email,
          password: userData.password,
        })
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(201);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('message');
          expect(res.body)
            .have
            .property('error')
            .eq(false);
          done();
        });
    });
  });
  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
