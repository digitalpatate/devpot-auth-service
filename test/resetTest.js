process.env.NODE_ENV = 'test';
const mongoose = require('mongoose');
const chai = require('chai');
const chaiHttp = require('chai-http');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


const User = require('../models/userModel');

const app = require('../app');

chai.use(chaiHttp);


const { expect, should, assert } = chai;
const userData = {
  id: '',
  username: 'testUserLogin',
  password: 'password',
  email: 'test@test.com',
  hash: '',
};

const frontUrl = 'localhost';

describe('Tests for the /users/reset', () => {
  beforeEach((done) => {
    userData.hash = bcrypt.hashSync(userData.password, 8);
    newUser = new User({
      username: userData.username,
      email: userData.email,
      password: userData.hash,
      status: 'validated',
    });
    newUser.save((err) => {
      if (err) console.log(err);
    });
    userData.id = newUser._id;
    done();
  });
  it('Try to reset status of an non-existing user', (done) => {
    chai.request(app)
      .post('/users/reset')
      .send({ email: 'nope@nope.nope', frontUrl })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Try to reset and no provide the email', (done) => {
    chai.request(app)
      .post('/users/reset')
      .send({ frontUrl })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Try to reset and no provide the front url', (done) => {
    chai.request(app)
      .post('/users/reset')
      .send({ email: 'nope@nope.nope' })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Try to reset status of an existing user', (done) => {
    chai.request(app)
      .post('/users/reset')
      .send({ email: userData.email, frontUrl })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(false);

        User.findOne({ email: userData.email }, (findErr, user) => {
          assert.equal(user.status, 'pending-reset');
          done();
        });
      });
  });
  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
describe('Tests for the /users/:id/reset', () => {
  const newPassword = 'newpassword';
  beforeEach((done) => {
    userData.hash = bcrypt.hashSync(userData.password, 8);
    const newUser = new User({
      username: userData.username,
      email: userData.email,
      password: userData.hash,
      status: 'pending-reset',
    });
    newUser.save((err) => {
      if (err) console.log(err);
    });
    userData.id = newUser._id;
    userData.token = jwt.sign({ _id: userData.id, username: 'test' }, process.env.JWT_SECRET, { expiresIn: '1d' });
    done();
  });
  it('Try to reset password and not given a password', (done) => {
    chai.request(app)
      .post(`/users/${userData.id}/reset`)
      .send({ token: userData.token })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
      });
    done();
  });
  it('Try to reset password and not given a the second password', (done) => {
    chai.request(app)
      .post(`/users/${userData.id}/reset`)
      .send({ password: newPassword, token: userData.token })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
      });
    done();
  });
  it('Try to reset password where password and password2 does not match', (done) => {
    chai.request(app)
      .post(`/users/${userData.id}/reset`)
      .send({ password: newPassword, token: userData.token })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
      });
    done();
  });
  it('Try to reset password and not provide the token', (done) => {
    chai.request(app)
      .post(`/users/${userData.id}/reset`)
      .send({ password: newPassword, password2: newPassword })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
      });
    done();
  });
  it('Should reset the password without errors', (done) => {
    chai.request(app)
      .post(`/users/${userData.id}/reset`)
      .send({ password: newPassword, password2: newPassword, token: userData.token })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(false);
      });
    done();
  });
});
