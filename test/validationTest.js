process.env.NODE_ENV = 'test';
const mongoose = require('mongoose');
const chai = require('chai');
const chaiHttp = require('chai-http');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


const User = require('../models/userModel');

const app = require('../app');

chai.use(chaiHttp);

const { expect } = chai;
const { should } = chai;
const userData = {
  id: '',
  username: 'testUserLogin',
  password: 'password',
  email: 'test@test.com',
  hash: '',
};
describe('Tests for the validation route', () => {
  beforeEach((done) => {
    userData.hash = bcrypt.hashSync(userData.password, 8);
    newUser = new User({
      username: userData.username,
      email: userData.email,
      password: userData.hash,
      status: 'pending-validate',
    });
    newUser.save((err) => {
      if (err) console.log(err);
    });
    userData.id = newUser._id;
    done();
  });
  it('Try to validate without token in body', (done) => {
    chai.request(app)
      .post(`/users/${userData.id}/tokens/validate`)
      .send()
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Try to validate with a non-valid signature token', (done) => {
    const token = jwt.sign({ nextStatus: 'validated' }, 'wrongSecret', { expiresIn: '1d' });
    chai.request(app)
      .post(`/users/${userData.id}/tokens/validate`)
      .send({ token })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Try to validate with a outdated token', (done) => {
    const token = jwt.sign({ nextStatus: 'validated', exp: Math.floor(Date.now() / 1000) - (60 * 60) }, process.env.JWT_SECRET);
    chai.request(app)
      .post(`/users/${userData.id}/tokens/validate`)
      .send({ token })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  describe('Validate a new registered user', () => {
    it('Should change status of the user', (done) => {
      const token = jwt.sign({ data: { nextStatus: 'validated' } }, process.env.JWT_SECRET, { expiresIn: '1d' });
      chai.request(app)
        .post(`/users/${userData.id}/tokens/validate`)
        .send({ token })
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(200);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('message');
          expect(res.body)
            .have
            .property('error')
            .eq(false);
          done();
        });
    });
  });
  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
