process.env.NODE_ENV = 'test';
const mongoose = require('mongoose');
const chai = require('chai');
const chaiHttp = require('chai-http');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


const User = require('../models/userModel');

const app = require('../app');

chai.use(chaiHttp);


const { expect, should, assert } = chai;
const userData = {
  id: '',
  username: 'testUserLogin',
  password: 'password',
  email: 'test@test.com',
  hash: '',
  token: '',
};

const frontUrl = 'localhost';

describe('Tests for the DELETE /users/:id', () => {
  beforeEach((done) => {
    userData.hash = bcrypt.hashSync(userData.password, 8);
    newUser = new User({
      username: userData.username,
      email: userData.email,
      password: userData.hash,
      status: 'validated',
    });
    newUser.save((err) => {
      if (err) console.log(err);
    });
    userData.id = newUser._id;
    userData.token = jwt.sign({ _id: userData.id, username: userData.username }, process.env.JWT_SECRET, { expiresIn: process.env.JWT_VALIDITY, issuer: 'localhost' });
    done();
  });
  it('Try to remove an non-existing user', (done) => {
    chai.request(app)
      .delete('/users/10')
      .set('Authorization', `Bearer ${userData.token}`)
      .send({ frontUrl })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Try to remove and no provide the front url', (done) => {
    chai.request(app)
      .delete(`/users/${userData.id}`)
      .set('Authorization', `Bearer ${userData.token}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Try to remove status of an existing user', (done) => {
    chai.request(app)
      .delete(`/users/${userData.id}`)
      .set('Authorization', `Bearer ${userData.token}`)
      .send({ frontUrl })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('message');
        expect(res.body)
          .have
          .property('error')
          .eq(false);

        User.findOne({ email: userData.email }, (findErr, user) => {
          assert.equal(user.status, 'pending-removed');
          done();
        });
      });
  });
  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
