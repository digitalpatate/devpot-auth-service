const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const { sendValidationToken } = require('../helpers/mail');

const userModel = mongoose.model('Users');

const { handleError, ErrorHandler } = require('../helpers/error');

/**
 * @swagger
 * /user/:userId:
 *   get:
 *     tags:
 *     - "User"
 *     summary: "Get a specific user"
 *     produces:
 *     - "application/json"
 *     security:
 *      - JWT : []
 *     parameters:
 *     - name: "userId"
 *       in: "path"
 *       description: "The unique id of the user"
 *       required: true
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       401:
 *         description: "Invalid authentication"
 *       403:
 *         description: "Access denied "
 *       404:
 *         description: "user not found"
 *       200:
 *         description: "Successfully returned"
 */
exports.userGet = (req, res) => {
  userModel.findById(req.params.id, (err, user) => {
    if (err) throw new ErrorHandler(400, err.message);
    if (!user) return handleError(new ErrorHandler(404, 'User not found'), res);
    user.set('password');
    if (req.decoded._id === user._id.toString()) return res.json({ data: user, error: false, message: '' });
    throw new ErrorHandler(403, 'Access denied');
  });
};

/**
 * @swagger
 * /user/:userId:
 *   put:
 *     tags:
 *     - "User"
 *     summary: "Update a specific user"
 *     produces:
 *     - "application/json"
 *     security:
 *      - JWT : []
 *     parameters:
 *     - name: "userId"
 *       in: "path"
 *       description: "The unique id of the user"
 *       required: true
 *       type: "string"
 *     - name: "frontUrl"
 *       in: "formData"
 *       description: "The frontend url used to create tha validation link"
 *       required: true
 *       type: "string"
 *     - name: "email"
 *       in: "formData"
 *       description: "The new user's email"
 *       required: false
 *       type: "string"
 *     - name: "username"
 *       in: "formData"
 *       description: "The new user's username"
 *       required: false
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       401:
 *         description: "Invalid authentication"
 *       403:
 *         description: "Access denied "
 *       404:
 *         description: "user not found"
 *       200:
 *         description: "Successfully registered"
 */
exports.userUpdate = (req, res) => {
  const {
    email,
    frontUrl,
    password,
  } = req.body;

  if (!frontUrl) throw new ErrorHandler(400, 'You should provide the front url');
  if (password) throw new ErrorHandler(400, 'You can\'t update your password this way');
  userModel.findOneAndUpdate({ _id: req.params.id }, req.body, (err, user) => {
    if (err) return handleError(new ErrorHandler(400, err.message), res);
    if (email) {
      user.status = 'pending-validated';
      user.save((saveError) => {
        if (saveError) return handleError(new ErrorHandler(400, saveError.errors), res);

        const validationToken = jwt.sign({ data: { userId: user._id, nextStatus: 'validated' } }, process.env.JWT_SECRET, { expiresIn: '1d' });
        sendValidationToken(user.email, 'Please validate your new Email address', frontUrl, user.id, validationToken);
        return res.status(200).json({ error: false, message: 'Email successfully updated', data: user });
      });
    }
    return res.status(200).json({ error: false, message: 'Successfully updated', data: user });
  });
};

/**
 * @swagger
 * /user/:userId:
 *   delete:
 *     tags:
 *     - "User"
 *     summary: "Delete a specific user"
 *     security:
 *      - JWT : []
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "userId"
 *       in: "path"
 *       description: "The unique id of the user"
 *       required: true
 *       type: "string"
 *     - name: "frontUrl"
 *       in: "formData"
 *       description: "The frontend url used to create tha validation link"
 *       required: true
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       401:
 *         description: "Invalid authentication"
 *       403:
 *         description: "Access denied "
 *       404:
 *         description: "user not found"
 *       200:
 *         description: "Successfully registered"
 */
exports.userDelete = (req, res) => {
  const { frontUrl } = req.body;
  if (!frontUrl) throw new ErrorHandler(400, 'You should provide the front url');

  userModel.findById(req.params.id, (findErr, user) => {
    if (!user || findErr) return handleError(new ErrorHandler(400, 'We did not find that user'), res);
    if (req.decoded._id !== user._id.toString()) return handleError(new ErrorHandler(403, 'Access denied'), res);
    user.status = 'pending-removed';
    user.save((saveError) => {
      if (saveError) {
        return res.status(200).json(
          { data: saveError, error: true, message: saveError.errors },
        );
      }
      const validationToken = jwt.sign({ data: { userId: user._id, nextStatus: 'removed' } }, process.env.JWT_SECRET, { expiresIn: '1d' });
      sendValidationToken(user.email, 'Confirm account suppression', frontUrl, user.id, validationToken);
      return res.status(200).json({ error: false, message: 'Please check pour mailbox', data: user });
    });
  });
};

/**
 * @swagger
 * /user/reset:
 *   post:
 *     tags:
 *     - "User"
 *     summary: "Request a link to reset the password, the link is sent by email"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "frontUrl"
 *       in: "formData"
 *       description: "The frontend url used to create tha validation link"
 *       required: true
 *       type: "string"
 *     - name: "email"
 *       in: "formData"
 *       description: "The user's email"
 *       required: true
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       404:
 *         description: "user not found"
 *       200:
 *         description: "Successfully reset"
 */
exports.resetPasswordChangeStatus = (req, res) => {
  const { email, frontUrl } = req.body;

  if (!email) throw new ErrorHandler(400, { email: 'Email is required' });

  if (!frontUrl) throw new ErrorHandler(400, 'You should provide the front url');

  userModel.findOne({ email }, (findError, user) => {
    if (!user || findError) return handleError(new ErrorHandler(400, 'We did not find your email, do you have an account ?'), res);

    user.status = 'pending-reset';
    user.save((saveError) => {
      if (saveError) {
        return res.status(200).json(
          { data: saveError, error: true, message: saveError.errors },
        );
      }

      const resetToken = jwt.sign({ data: { userId: user._id, nextStatus: 'validated' } }, process.env.JWT_SECRET, { expiresIn: '1d' });
      sendValidationToken(user.email, 'Reset password', frontUrl, user.id, resetToken);
      return res.status(200).json({ error: false, message: 'Please check pour mailbox', data: user });
    });
  });
};

/**
 * @swagger
 * /user/:userId/reset:
 *   post:
 *     tags:
 *     - "User"
 *     summary: "Reset a specific user's password"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "userId"
 *       in: "path"
 *       description: "The unique id of the user"
 *       required: true
 *       type: "string"
 *     - name: "password"
 *       in: "formData"
 *       description: "The new password"
 *       required: true
 *       type: "string"
 *     - name: "password2"
 *       in: "formData"
 *       description: "The new password repeated"
 *       required: true
 *       type: "string"
 *     - name: "token"
 *       in: "formData"
 *       description: "The token you receive in get param"
 *       required: true
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       404:
 *         description: "user not found"
 *       403:
 *         description: "Access denied"
 *       200:
 *         description: "Successfully reset"
 */
exports.resetPassword = (req, res) => {
  const { password, password2, token } = req.body;

  if (!password || !password2) throw new ErrorHandler(400, { password: 'Password is required', password2: 'Please repeat the password' });

  if (!token) throw new ErrorHandler(400, 'You should provide a token');

  if (password !== password2) throw new ErrorHandler(400, { password: 'Passwords does not match' });

  const decoded = jwt.verify(token, process.env.JWT_SECRET);
  if (!decoded) throw new ErrorHandler(400, { token: 'Token is not valid' });

  if (decoded._id !== req.params.id.toString()) return handleError(new ErrorHandler(403, 'Access denied'), res);

  userModel.findById(req.params.id, (findErr, user) => {
    if (!user || findErr) return handleError(new ErrorHandler(404, 'We did not find you, do you have an account ?'), res);
    if (user.status !== 'pending-reset') return handleError(new ErrorHandler(400, 'Before you reset your password, you have to send the reset request'), res);
    user.password = password;
    user.status = decoded.status;
    user.save((saveErr) => {
      if (saveErr) throw new ErrorHandler(400, saveErr.errors);
      return res.json({ error: false, data: '', message: 'Password changed !' });
    });
  });
};

/**
 * @swagger
 * /user/:userId/tokens/validate:
 *   post:
 *     tags:
 *     - "User"
 *     summary: "Validate the token"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "userId"
 *       in: "path"
 *       description: "The unique id of the user"
 *       required: true
 *       type: "string"
 *     - name: "token"
 *       in: "formData"
 *       description: "A valid JWT token"
 *       required: true
 *       type: "string"
 *     - name: "frontUrl"
 *       in: "formData"
 *       description: "The frontend url used to create tha validation link"
 *       required: true
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       404:
 *         description: "user not found"
 *       200:
 *         description: "Successfully reset"
 */
exports.validate = function (req, res) {
  const { token } = req.body;
  if (!token) throw new ErrorHandler(400, 'You should provide a token');

  const decodedToken = jwt.verify(token, process.env.JWT_SECRET, (err, data) => {
    if (err) throw new ErrorHandler(400, 'Token is not valid');
    return data;
  });
  userModel.findById(req.params.id, (err, user) => {
    if (err) throw new ErrorHandler(400, err.message);
    user.status = decodedToken.data.nextStatus;

    user.save((saveErr) => {
      if (saveErr) throw new ErrorHandler(400, saveErr.errors);
      return res.json({ error: false, data: '', message: 'validated' });
    });
  });
};

/**
 * @swagger
 * /user/:userId/tokens/renew:
 *   post:
 *     tags:
 *     - "User"
 *     summary: "Renew the given token"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "userId"
 *       in: "path"
 *       description: "The unique id of the user"
 *       required: true
 *       type: "string"
 *     - name: "oldToken"
 *       in: "formData"
 *       description: "A valid outdated JWT token"
 *       required: true
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       404:
 *         description: "user not found"
 *       200:
 *         description: "Successfully renew"
 */
exports.renewToken = (req, res) => {
  const {
    oldToken,
    frontUrl,
  } = req.body;


  if (!oldToken) throw new ErrorHandler(400, 'You should provide the old token');
  if (!frontUrl) throw new ErrorHandler(400, 'You should provide the front url');

  userModel.findOne({ _id: req.params.id }, (err, user) => {
    if (err) throw new ErrorHandler(400, 'Nope');

    jwt.verify(oldToken, process.env.JWT_SECRET, { ignoreExpiration: true }, (jwtDecodeErr) => {
      if (jwtDecodeErr) throw new ErrorHandler(400, 'Token is not valid');
    });
    const payload = jwt.decode(oldToken);

    const validationToken = jwt.sign(payload.data, process.env.JWT_SECRET, { expiresIn: '1d' });
    sendValidationToken(user.email, 'Your new token', frontUrl, user.id, validationToken);
    return res.status(200).json({ error: false, message: 'A new token has been send to your email address' });
  });
};
