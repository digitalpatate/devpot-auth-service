const passport = require('passport');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');

const { sendValidationToken } = require('../helpers/mail');

const { handleError, ErrorHandler } = require('../helpers/error');


const UserModel = mongoose.model('Users');


/**
 * @swagger
 * /register:
 *   post:
 *     tags:
 *     - "Auth"
 *     summary: "Register a user to the application"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "username"
 *       in: "formData"
 *       description: "The user's username"
 *       required: true
 *       type: "string"
 *     - name: "email"
 *       in: "formData"
 *       description: "The user's email"
 *       required: true
 *       type: "string"
 *     - name: "password"
 *       in: "formData"
 *       description: "The user's password"
 *       required: true
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       200:
 *         description: "Successfully registered"
 */
exports.register = (req, res) => {
  const {
    username,
    email,
    password,
    frontUrl,
  } = req.body;

  if (!username) throw new ErrorHandler(400, { username: 'Username is required' });
  if (!password) throw new ErrorHandler(400, { password: 'Password is required' });

  const hashedPassword = bcrypt.hashSync(password, 8);

  const newUser = new UserModel({
    username,
    email,
    password: hashedPassword,
  });
  newUser.save((err) => {
    if (err) return handleError(new ErrorHandler(400, err.errors), res);
    newUser.set('password');
    const validationToken = jwt.sign({ data: { userId: newUser._id, nextStatus: 'validated' } }, process.env.JWT_SECRET, { expiresIn: '1d' });
    sendValidationToken(newUser.email, 'Please validate your Email address', frontUrl, newUser.id, validationToken);
    return res.status(201).json({ error: false, message: 'Successfully register, please valid your email address', data: newUser });
  });
};

/**
 * @swagger
 * /login:
 *   post:
 *     tags:
 *     - "Auth"
 *     summary: "log a user in (generate a JWT token)"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "username"
 *       in: "formData"
 *       description: "The user's username"
 *       required: true
 *       type: "string"
 *     - name: "password"
 *       in: "formData"
 *       description: "The user's password"
 *       required: true
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       200:
 *         description: "Successfully logged in"
 */
exports.login = (req, res) => {
  passport.authenticate('local', { session: false }, (authenticateError, user, info) => {
    if (authenticateError || !user) {
      return res.status(400).json({
        message: info ? info.message : 'Login failed',
        data: user,
        error: true,
      });
    }
    req.login(user, { session: false }, (loginError) => {
      if (loginError) {
        res.send(loginError);
      }
      user.set('password');
      const token = jwt.sign({ _id: user._id, username: user.username }, process.env.JWT_SECRET, { expiresIn: process.env.JWT_VALIDITY, issuer: 'localhost' });
      return res.status(200).json({
        token, data: user, message: 'Successfully logged in', error: false,
      });
    });
  })(req, res);
};
