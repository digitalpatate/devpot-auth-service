FROM node:latest
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
RUN npm install
COPY . /usr/src/app

EXPOSE 3000

ENV DB_URL=localhost
ENV DB_PORT=27017
ENV DB_NAME=authentification-service
ENV NODE_ENV=dev
ENV NODE_BASEPATH=/api/v1
ENV SWAGGER_BASEPATH=/api/v1
ENV PORT=3000

CMD npm start

